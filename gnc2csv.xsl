<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
 
<xsl:template match="/"><xsl:apply-templates/></xsl:template>
<xsl:template match="table"><xsl:text>&#10;</xsl:text><xsl:apply-templates/></xsl:template>
<xsl:template match="tbody"><xsl:apply-templates/></xsl:template>
<xsl:template match="thead"><xsl:apply-templates/></xsl:template>

<xsl:template match="tr">
<xsl:apply-templates/><xsl:text>&#10;</xsl:text></xsl:template>

<xsl:template match="td">
<xsl:value-of select="."/><xsl:text>;</xsl:text></xsl:template>
<xsl:template match="th">
<xsl:value-of select="."/><xsl:text>;</xsl:text></xsl:template>
<xsl:template match="h3">
<xsl:value-of select="."/><xsl:text>;</xsl:text></xsl:template>



<xsl:template match="a"/>
<xsl:template match="style"/>
<xsl:template match="title"/>

<xsl:strip-space elements="*"/>


</xsl:stylesheet>